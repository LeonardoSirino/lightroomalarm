#include "Arduino.h"
#include "Preferences.h"
#include "time.h"

#include "ArduinoJson.h"
#include "mbedtls/md.h"

#include "AsyncUDP.h"
#include "WiFi.h"
#include "WiFiUdp.h"

#include "NTPClient.h"

// Classe UDP
AsyncUDP udp;

// Infos da rede
const char *networkName = "Red 58 2.4G";
const char *networkPswd = "11112017";
// const char *networkName = "LeoS";
// const char *networkPswd = "leo270296";

const int udpPort = 3333;
bool connected = false;
const char *ESP_IDLE_MESSAGE = "esp_idle_message";

// Configurações do servido NTP
const char *ntpServer = "south-america.pool.ntp.org";
const long gmtOffset_sec = -3 * 3600;
const int daylightOffset_sec = 3600;
long int usec_last_update = 0;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, ntpServer, gmtOffset_sec);

// Pinos para controlar a fita LED
#define enA 2
#define in1 18
#define in2 19

// Parâmetros do PWM
#define PWM_CHANNEL_0 0
#define PWM_TIMER 13
#define PWM_BASE_FREQ 2000

// Variáveis para o controle manual do LED
volatile float lightIntensity = 50.0;
volatile bool lightStatus = false;

// alarmes
JsonArray alarmData;
float full_light_duration = 5;
volatile bool alarm_active = false;
Preferences preferences;

DynamicJsonDocument receivedDoc(5000);
DynamicJsonDocument alarmDataDoc(5000);
DynamicJsonDocument sendDoc(5000);
DynamicJsonDocument hashDoc(5000);

struct parse_data_return
{
    boolean return_hash;
    boolean return_sendDoc;
};

void setup()
{
    Serial.begin(115200);

    connectToWiFi(networkName, networkPswd);
    delay(500);

    timeClient.begin();

    setup_light_control();

    preferences.begin("my-app", false);
    read_local_alarm_data();

    if (udp.listenMulticast(IPAddress(239, 1, 2, 3), udpPort))
    {
        Serial.print("UDP Listening on IP: ");
        Serial.println(WiFi.localIP());
        // Definição do callback para os pacotes recebidos
        udp.onPacket(received_packet_callback);
    }

    // Inicio da tarefa de controle dos alarmes
    xTaskCreatePinnedToCore(
        secondary_core_main_loop,   /* Function to implement the task */
        "secondary_core_main_loop", /* Name of the task */
        5000,                       /* Stack size in words */
        NULL,                       /* Task input parameter */
        1,                          /* Priority of the task */
        NULL,                       /* Task handle. */
        1);                         /* Core where the task should run */
}

void loop()
{
    delay(500);
    udp.broadcastTo(ESP_IDLE_MESSAGE, udpPort);
    // Serial.println("Broadcast sent");

    control_alarms();
}

void control_alarms()
{
    float day_minutes = get_day_minutes();
    // Serial.println(day_minutes);

    int day_index = timeClient.getDay();

    float progress = calc_alarm_progress(day_minutes, day_index);
    if (progress > 0)
    {
        write_light_intensity(progress);
        alarm_active = true;
    }
    else if (progress == 0.0 and !lightStatus)
    {
        alarm_active = false;
        lightIntensity = 50;
    }
    else
    {
        alarm_active = false;
    }
}

void secondary_core_main_loop(void *pvParameters)
{
    while (true)
    {
        // Place holder loop for future features
        delay(1000);
    }
}

void received_packet_callback(AsyncUDPPacket packet)
{
    // Conversão do uint_8t para um objeto String
    char *tmpStr = (char *)malloc(packet.length() + 1);
    memcpy(tmpStr, packet.data(), packet.length());
    tmpStr[packet.length()] = '\0'; // ensure null termination
    String data = String(tmpStr);
    free(tmpStr); // String(char*) creates a copy so we can delete our one

    parse_data_return res = parse_data(data);

    if (res.return_hash)
    {
        hashDoc["operation"] = "hash";
        hashDoc["hash"] = sha1_hash_string(data);
        String hashDoc_string = "";
        serializeJson(hashDoc, hashDoc_string);

        const char *hash = hashDoc_string.c_str();
        packet.printf(hash, sizeof(hash));

        Serial.println("Returned hash to client:");
        Serial.println(hashDoc_string);
    }

    if (res.return_sendDoc)
    {
        String sendDoc_string = "";
        serializeJson(sendDoc, sendDoc_string);

        const char *data = sendDoc_string.c_str();
        packet.printf(data, sizeof(data));

        Serial.println("Send data to client:");
        Serial.println(sendDoc_string);
    }

    Serial.println("parse_data function has ended");
}

/**
 * @brief Parse the received packet, execute functions e return if hash should be sended back
 * 
 * @param data data Packet data
 * @return parse_data_return Return hash to client / return sendDoc to client
 */
parse_data_return parse_data(String data)
{
    // Deserialize to received object
    deserializeJson(receivedDoc, data);
    JsonObject obj = receivedDoc.as<JsonObject>();

    String operation = obj["operation"];

    parse_data_return response;
    response.return_hash = false;
    response.return_sendDoc = false;

    Serial.println("Chamando funcao correspondente");
    if (operation == "setLightIntensity")
    {
        if (!alarm_active)
        {
            lightIntensity = obj["lightIntensity"];
            Serial.print("Set lightIntensity: ");
            Serial.println(lightIntensity);

            if (lightStatus)
            {
                write_light_intensity(lightIntensity);
            }
        }
        else
        {
            Serial.println("Active alarm, light intensity will not be used");
        }
    }
    else if (operation == "setLightStatus")
    {
        lightStatus = obj["lightStatus"];
        Serial.print("Set lightStatus: ");
        Serial.println(lightStatus);

        response.return_hash = true;
    }
    else if (operation == "setAlarmData")
    {
        Serial.println("Nova configuracao de alarmes");

        // Deserialize to alarmData global object
        deserializeJson(alarmDataDoc, data);
        JsonObject alarmDataObj = alarmDataDoc.as<JsonObject>();

        alarmData = alarmDataObj["allAlarmData"];
        serializeJsonPretty(alarmData, Serial);

        Serial.println("Salvando dados na memoria");
        preferences.putString("alarm-data", data);

        light_feedback();

        response.return_hash = true;
    }
    else if (operation == "getLightIntensity")
    {
        sendDoc.clear();
        sendDoc["operation"] = operation;
        sendDoc["lightIntensity"] = lightIntensity;

        response.return_sendDoc = true;
    }
    else
    {
        Serial.println("Invalid operation");
    }

    return response;
}

/**
 * @brief Setup da escrita no LED com PWM
 * 
 */
void setup_light_control()
{
    Serial.println("Set pins");
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);

    Serial.println("Pins write");
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);

    Serial.println("Set analog pin");
    ledcSetup(PWM_CHANNEL_0, PWM_BASE_FREQ, PWM_TIMER);
    ledcAttachPin(enA, PWM_CHANNEL_0);

    // for (int i = 0; i < 3; i++)
    // {
    //     write_light_intensity(1);
    //     delay(200);
    //     write_light_intensity(0);
    //     delay(200);
    // }
}

/**
 * @brief Função para escrever a intensidade luminosa como PWM na saída analógica
 * 
 * @param value O valor de entrada deve ser entre 0 e 100
 */
void write_light_intensity(float value)
{
    if (value < 0)
    {
        value = 0;
    }
    else if (value > 100)
    {
        value = 100;
    }

    // a progressão numa função cúbica é mais agradável
    value = pow(value / 100, 3);

    // calculate duty, 8191 from 2 ^ 13 - 1
    uint32_t duty = 8191 * value;

    ledcWrite(PWM_CHANNEL_0, duty);
}

void connectToWiFi(const char *ssid, const char *pwd)
{
    Serial.println("Connecting to WiFi network: " + String(ssid));

    WiFi.disconnect(true);
    WiFi.onEvent(WiFiEvent);
    WiFi.begin(ssid, pwd);

    Serial.println("Waiting for WIFI connection...");
}

void WiFiEvent(WiFiEvent_t event)
{
    switch (event)
    {
    case SYSTEM_EVENT_STA_GOT_IP:
        //When connected set
        Serial.print("WiFi connected! IP address: ");
        Serial.println(WiFi.localIP());
        connected = true;
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        connected = false;
        break;
    }
}

String sha1_hash_string(String payload_string)
{
    const char *payload = payload_string.c_str();
    byte shaResult[32];

    mbedtls_md_context_t ctx;
    mbedtls_md_type_t md_type = MBEDTLS_MD_SHA256;

    const size_t payloadLength = strlen(payload);

    mbedtls_md_init(&ctx);
    mbedtls_md_setup(&ctx, mbedtls_md_info_from_type(md_type), 0);
    mbedtls_md_starts(&ctx);
    mbedtls_md_update(&ctx, (const unsigned char *)payload, payloadLength);
    mbedtls_md_finish(&ctx, shaResult);
    mbedtls_md_free(&ctx);

    String hash = "";

    for (int i = 0; i < sizeof(shaResult); i++)
    {
        char str[3];

        sprintf(str, "%02x", (int)shaResult[i]);
        // Serial.print(str);

        hash += String(str);
    }

    return hash;
}

float get_day_minutes()
{
    timeClient.update();
    float day_seconds = (float)timeClient.getSeconds();
    day_seconds += (float)timeClient.getMinutes() * 60.0;
    day_seconds += (float)timeClient.getHours() * 3600.0;

    float day_minutes = day_seconds / 60.0;
    return day_minutes;
}

float calc_alarm_progress(float day_minutes, int day_index)
{
    for (JsonObject item : alarmData)
    {
        bool day_active = item["days"][day_index];

        if (day_active)
        {
            int initMinutes = item["initMinutes"];
            int endMinutes = item["endMinutes"];

            if (day_minutes >= initMinutes and day_minutes <= endMinutes)
            {
                float progress = (day_minutes - initMinutes) / (endMinutes - initMinutes) * 100;
                return progress;
            }
            else if (day_minutes > endMinutes and day_minutes <= (endMinutes + full_light_duration))
            {
                return 100.0;
            }
        }
    }
    return 0.0;
}

void read_local_alarm_data()
{
    String data = preferences.getString("alarm-data", "");
    Serial.println("Dados lidos da memória");
    Serial.println(data);
    if (data != "")
    {
        parse_data(data);
    }
}

void light_feedback()
{
    float duration = 500; // milis
    float max_intensity = 50;
    int steps = 1000;

    float increment = max_intensity / float(steps) / 2.0;
    uint32_t delay_time_ms = duration / steps / 2;

    for (float j = 0.0; j < max_intensity; j += increment)
    {
        write_light_intensity(j);
        delay(delay_time_ms);
    }

    for (float j = max_intensity; j > 0; j -= increment)
    {
        write_light_intensity(j);
        delay(delay_time_ms);
    }
}