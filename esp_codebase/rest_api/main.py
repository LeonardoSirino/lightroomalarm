import requests
import time
import numpy as np

url = 'http://lightroomalarm.local/'

ip_adress = requests.get(url).text
print('Identificado ip: {}'.format(ip_adress))

url = 'http://{}/'.format(ip_adress)
print(url)

pings = []
for _ in range(50):
    res = requests.get(url)
    mili_ping = res.elapsed.microseconds / 1000
    pings.append(mili_ping)

print(pings)
print('Ping: {:.2f} +- {:.2f} ms'.format(np.mean(pings), np.std(pings)))
