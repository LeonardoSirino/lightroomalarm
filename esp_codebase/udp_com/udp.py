import socket
import time

UDP_PORT = 3333

client = socket.socket(socket.AF_INET,
                       socket.SOCK_DGRAM,
                       socket.IPPROTO_UDP)  # UDP

client.setsockopt(socket.SOL_SOCKET,
                  socket.SO_BROADCAST, 1)
                  
client.bind(("", UDP_PORT))

message = b"ola do Python"

while True:
    client.sendto(message, ('<broadcast>', UDP_PORT))

    data, addr = client.recvfrom(1024)
    print("received message: %s" % data)
    print("de: ")
    print(addr)

    print('\n')

    time.sleep(1)
