#include "Arduino.h"
#include "time.h"
#include <WiFi.h>
#include "AsyncUDP.h"
#include <ArduinoJson.h>

// Classe UDP
AsyncUDP udp;

// Infos da rede
const char *networkName = "JeremySpokeIn";
const char *networkPswd = "harmless";
const int udpPort = 3333;

DynamicJsonDocument doc(1024);

void setup()
{
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin(networkName, networkPswd);
    if (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
        Serial.println("WiFi Failed");
        while (1)
        {
            delay(1000);
        }
    }
    if (udp.listenMulticast(IPAddress(239, 1, 2, 3), udpPort))
    {
        Serial.print("UDP Listening on IP: ");
        Serial.println(WiFi.localIP());
        // Definição do callback para os pacotes recebidos
        udp.onPacket(received_packet_callback);
    }
}

void loop()
{
    delay(200);

    udp.broadcastTo("esp_idle_message", udpPort);
    // Serial.println("Broadcast sent");
}

void received_packet_callback(AsyncUDPPacket packet)
{
    char *tmpStr = (char *)malloc(packet.length() + 1);
    memcpy(tmpStr, packet.data(), packet.length());
    tmpStr[packet.length()] = '\0'; // ensure null termination
    String data = String(tmpStr);
    free(tmpStr); // String(char*) creates a copy so we can delete our one

    // implementar o ECHO!!!
    // echo to client
    // packet.printf(packet.data(), packet.length());

    parse_data(data);
}

void parse_data(String data)
{
    Serial.println(data);

    deserializeJson(doc, data);
    JsonObject obj = doc.as<JsonObject>();

    // // Lastly, you can print the resulting JSON to a String
    // String output;
    // serializeJson(doc, output);

    // Serial.print("JSON: ");
    // Serial.println(output);
}