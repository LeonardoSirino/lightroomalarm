#include <iostream>
#include "ArduinoJson.h"

JsonArray alarmData;

float full_light_duration = 5;

float calc_progress(float day_minutes, int day_index)
{
  for (JsonVariant item : alarmData)
  {
    bool day_active = item["days"][day_index];
    if (day_active)
    {
      int initMinutes = item["initMinutes"];
      int endMinutes = item["endMinutes"];

      if (day_minutes >= initMinutes and day_minutes <= endMinutes)
      {
        std::cout << "ATIVO!" << std::endl;
        float progress = (day_minutes - initMinutes) / (endMinutes - initMinutes) * 100;
        return progress;
      }
      else if (day_minutes > endMinutes and day_minutes <= (endMinutes + full_light_duration))
      {
        std::cout << "FULL LIGHT!" << std::endl;
        return 100.0;
      }
    }
  }
  return 0.0;
}

int main()
{
  DynamicJsonDocument doc(10000);

  char json[] =
      "{\"allAlarmData\":[{\"id\":0,\"days\":[false,true,true,true,true,true,false],\"initMinutes\":420,\"endMinutes\":450},{\"id\":1,\"days\":[true,false,false,false,false,false,true],\"initMinutes\":480,\"endMinutes\":510}],\"operation\":\"setAlarmData\"}";

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, json);

  // Test if parsing succeeds.
  if (error)
  {
    std::cerr << "deserializeJson() failed: " << error.c_str() << std::endl;
    return 1;
  }

  alarmData = doc["allAlarmData"];

  for (float j = 420.0; j < 480.0; j += 5.0)
  {
    float progress = calc_progress(j, 0);
    std::cout << progress << std::endl;
  }

  return 0;
}
