#include <NTPClient.h>
// change next line to use with another board/shield

#include <WiFi.h>
#include "time.h"
#include "Arduino.h"

#include <WiFiUdp.h>

// Infos da rede
const char *networkName = "JeremySpokeIn";
const char *networkPswd = "harmless";
boolean connected = false;

WiFiUDP ntpUDP;

// Configurações do servido NTP
const char *ntpServer = "south-america.pool.ntp.org";
const long gmtOffset_sec = -3 * 3600;
const int daylightOffset_sec = 3600;
long int usec_last_update = 0;

// By default 'pool.ntp.org' is used with 60 seconds update interval and
// no offset
NTPClient timeClient(ntpUDP, ntpServer, gmtOffset_sec);

// You can specify the time server pool and the offset, (in seconds)
// additionaly you can specify the update interval (in milliseconds).
// NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);

void setup()
{
    Serial.begin(115200);

    Serial.println("Connect to WiFi");
    connectToWiFi(networkName, networkPswd);
    delay(2000);

    timeClient.begin();
}

void loop()
{
    timeClient.update();

    Serial.println(timeClient.getFormattedTime());

    Serial.print("Horas: ");
    Serial.println(timeClient.getHours());
    Serial.print("Minutos: ");
    Serial.println(timeClient.getMinutes());

    delay(1000);
}

void connectToWiFi(const char *ssid, const char *pwd)
{
    Serial.println("Connecting to WiFi network: " + String(ssid));

    WiFi.disconnect(true);
    WiFi.onEvent(WiFiEvent);
    WiFi.begin(ssid, pwd);

    Serial.println("Waiting for WIFI connection...");
}

void WiFiEvent(WiFiEvent_t event)
{
    switch (event)
    {
    case SYSTEM_EVENT_STA_GOT_IP:
        //When connected set
        Serial.print("WiFi connected! IP address: ");
        Serial.println(WiFi.localIP());
        connected = true;
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        connected = false;
        break;
    }
}
