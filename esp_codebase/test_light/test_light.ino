#include <WiFi.h>
#include "time.h"
#include "Arduino.h"

// Configurações do despertador
const int wakeup_hour = 23;
const int wakeup_min = 20;
const int fade_time_minutes = 20;
const int on_time = 1;
const int blink_time = 1;

int wakeup_seconds_since_mn = wakeup_hour * 3600 + wakeup_min * 60;
int init_fade = wakeup_seconds_since_mn - fade_time_minutes * 60;
boolean done = false;

// Configurações do servido NTP
const char *ntpServer = "south-america.pool.ntp.org";
const long gmtOffset_sec = -3 * 3600;
const int daylightOffset_sec = 3600;
long int usec_last_update = 0;

// Pinos para controlar a fita LED
#define enA 2
#define in1 18
#define in2 19

// Parâmetros do PWM
#define PWM_CHANNEL_0 0
#define PWM_TIMER 13
#define PWM_BASE_FREQ 2000

// Infos da rede
const char *networkName = "JeremySpokeIn";
const char *networkPswd = "harmless";
boolean connected = false;

// Arduino like analogWrite
// value has to be between 0 and valueMax
void pwmAnalogWrite(uint8_t channel, float value)
{
    if (value < 0)
    {
        value = 0;
    }
    else if (value > 1)
    {
        value = 1;
    }

    // Serial.print("Value: ");
    // Serial.println(value);

    // calculate duty, 8191 from 2 ^ 13 - 1
    uint32_t duty = 8191 * value;

    // Serial.print("Duty: ");
    // Serial.println(duty);

    // write duty to LEDC
    ledcWrite(channel, duty);
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Inicio do setup");

    Serial.println("Set pins");
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);

    Serial.println("Pins write");
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);

    Serial.println("Set analog pin");
    ledcSetup(PWM_CHANNEL_0, PWM_BASE_FREQ, PWM_TIMER);
    ledcAttachPin(enA, PWM_CHANNEL_0);

    for (int i = 0; i < 3; i++)
    {
        pwmAnalogWrite(PWM_CHANNEL_0, 1);
        delay(200);
        pwmAnalogWrite(PWM_CHANNEL_0, 0);
        delay(200);
    }

    delay(500);

    Serial.println("Config time request");
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

    Serial.println("Connect to WiFi");
    connectToWiFi(networkName, networkPswd);
    delay(5000);
}

void loop()
{
    if (connected)
    {
        int seconds_smn = get_seconds_since_mn();
        if (seconds_smn > init_fade && seconds_smn < wakeup_seconds_since_mn)
        {
            float progress = (seconds_smn - init_fade) / (fade_time_minutes * 60.0);
            pwmAnalogWrite(PWM_CHANNEL_0, progress);
            Serial.print("Progresso: ");
            Serial.println(progress);
        }

        if (seconds_smn > wakeup_seconds_since_mn && seconds_smn < (wakeup_seconds_since_mn + on_time * 60.0))
        {
            done = true;
        }

        Serial.print("Segundos desde a meia noite: ");
        Serial.println(seconds_smn);

        Serial.print("Setpoint: ");
        Serial.println(init_fade);

        Serial.println("");

        if (done)
        {
            Serial.println("Iniciando on time");
            pwmAnalogWrite(PWM_CHANNEL_0, 1);
            delay(on_time * 60 * 1000);

            Serial.println("Iniciando blink time");
            for (int i = 0; i < 60 * blink_time; i++)
            {
                pwmAnalogWrite(PWM_CHANNEL_0, 1);
                delay(500);
                pwmAnalogWrite(PWM_CHANNEL_0, 0);
                delay(500);
            }

            done = false;
        }
    }
    else
    {
        connectToWiFi(networkName, networkPswd);
        delay(5000);
    }

    delay(1000);
}

//-------------------//
// FUNCTIONS
//-------------------//

void connectToWiFi(const char *ssid, const char *pwd)
{
    Serial.println("Connecting to WiFi network: " + String(ssid));

    WiFi.disconnect(true);
    WiFi.onEvent(WiFiEvent);
    WiFi.begin(ssid, pwd);

    Serial.println("Waiting for WIFI connection...");
}

void WiFiEvent(WiFiEvent_t event)
{
    switch (event)
    {
    case SYSTEM_EVENT_STA_GOT_IP:
        //When connected set
        Serial.print("WiFi connected! IP address: ");
        Serial.println(WiFi.localIP());
        connected = true;
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        connected = false;
        break;
    }
}

int get_seconds_since_mn()
{
    struct tm timeinfo;
    if (getLocalTime(&timeinfo))
    {
        // String year = String(timeinfo.tm_year + 1900);
        // String month = String(timeinfo.tm_mon + 1);
        // String day = String(timeinfo.tm_mday);
        // String hour = String(timeinfo.tm_hour);
        // String minute = String(timeinfo.tm_min);
        // String sec = String(timeinfo.tm_sec);
        // String time = year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + sec;

        // Serial.println(time);

        int seconds_smn = timeinfo.tm_hour * 3600 + timeinfo.tm_min * 60 + timeinfo.tm_sec;
        return seconds_smn;
    }
    else
    {
        return 0;
    }
}
