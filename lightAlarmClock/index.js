/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import App from './src';
AppRegistry.registerComponent(appName, () => App);

// import Temp from './src/pages/temp';
// AppRegistry.registerComponent(appName, () => Temp);