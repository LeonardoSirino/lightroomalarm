import { StatusBar } from "react-native";
import { Colors } from '../styles'

StatusBar.setBackgroundColor(Colors.Background)
StatusBar.setBarStyle('light-content')