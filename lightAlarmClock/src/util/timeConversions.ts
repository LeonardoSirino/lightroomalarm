export const minutesToStringHour = (minutes: number) => {
    const minute = minutes % 60
    const hour = Math.floor(minutes / 60)

    let minuteString = ''

    if (minute < 10) {
        minuteString = '0' + minute.toString()
    } else {
        minuteString = minute.toString()
    }

    return `${hour}:${minuteString}`
}

export const minutesToHourAndMinutes = (absoluteMinutes: number) => {
    const minute = absoluteMinutes % 60
    const hour = Math.floor(absoluteMinutes / 60)

    return { hour, minute }
}

export const nativeDateToMinutes = (date: Date) => {
    let minutes = date.getHours() * 60 + date.getMinutes()

    return minutes
}