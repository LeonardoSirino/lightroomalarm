import { Component } from "react";
import React from 'react'

import { minutesToStringHour } from "../util/timeConversions";

import { StyleSheet, Text, View, TouchableOpacity, FlatList } from "react-native";

import { Colors } from "../styles";

import { alarmDataType } from "../storage/types";
import { GlobalContext } from "../globalContext";

const daysLetters = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S']

export default class AlarmBlock extends Component {
    state = {
        selectionMode: false,
        selected: false
    }

    alarmData: alarmDataType = {
        days: [false, true, true, true, true, true, false],
        initMinutes: 7 * 60,
        endMinutes: 7.5 * 60,
        id: 0
    }

    constructor(props) {
        super(props)
        console.log('Ativação do constructor do AlarmBlock')

        this.updateAlarmData()
    }

    updateAlarmData = () => {
        let alarmData: alarmDataType
        try {
            alarmData = this.props.alarmData
            console.log('Recebidos os parâmetros para o bloco de alarme')
            console.log(alarmData)
        } catch{
            console.log('Não foram definidos os parâmetros do bloco de alarme, usando valores Default')
            alarmData = {
                days: [false, true, true, true, true, true, false],
                initMinutes: 7 * 60,
                endMinutes: 7.5 * 60,
                id: 0
            }
        }

        this.alarmData = alarmData
    }

    componentDidMount() {
        console.log('Montou block de config')
    }

    renderDay({ item }) {
        let dayLetterStyle = { ...styles.dayLetter }

        if (item.status) {
            dayLetterStyle.color = Colors.Purple

        } else {
            dayLetterStyle.color = Colors.Foreground
            dayLetterStyle.opacity = 0.5
        }
        return (
            <View style={styles.dayLetterContainer}>
                <Text style={dayLetterStyle}>{item.dayLetter}</Text>
            </View>
        )

    }

    longPressHandler = (setContextData) => {
        console.log('Chamada da longPressHandler')
        console.log(this.context)
        setContextData({ selectionMode: ~this.context.selectionMode })

        console.log('Tentativa de escrita no contexto')
        this.setState({ selected: ~this.state.selected })
    }

    renderDefault() {
        console.log('Render Alarm Block');
        this.updateAlarmData();
        const dayData = this.alarmData.days.map((value, index) => {
            let data = {
                dayLetter: daysLetters[index],
                status: value,
                id: index
            };

            return data;
        });

        const initTimeString = minutesToStringHour(this.alarmData.initMinutes);
        const endTimeString = minutesToStringHour(this.alarmData.endMinutes);

        console.log(dayData);
        return (
            <GlobalContext.Consumer>
                {({ setContextData }) => (
                    <TouchableOpacity
                        style={styles.container}
                        onPress={() => { this.props.editAlarmScreen(); }}
                        onLongPress={() => { this.longPressHandler(setContextData) }}>

                        <View style={styles.hourContainer}>
                            <Text style={styles.text}>{initTimeString}</Text>
                            <Text style={styles.text}>➙</Text>
                            <Text style={styles.text}>{endTimeString}</Text>
                        </View>

                        <FlatList
                            contentContainerStyle={styles.list}
                            data={dayData}
                            keyExtractor={item => item.id}
                            renderItem={this.renderDay}
                            horizontal={true} />
                    </TouchableOpacity>
                )}
            </GlobalContext.Consumer>
        );
    }

    renderSelectionMode() {
        console.log('Render Alarm Block');
        this.updateAlarmData();
        const dayData = this.alarmData.days.map((value, index) => {
            let data = {
                dayLetter: daysLetters[index],
                status: value,
                id: index
            };

            return data;
        });

        const initTimeString = minutesToStringHour(this.alarmData.initMinutes);
        const endTimeString = minutesToStringHour(this.alarmData.endMinutes);

        console.log(dayData);
        return (
            <GlobalContext.Consumer>
                {({ setContextData }) => (
                    <TouchableOpacity
                        style={styles.containerSelectionMode}
                        onLongPress={() => { this.longPressHandler(setContextData) }}
                    >

                        <View style={styles.hourContainer}>
                            <Text style={styles.text}>{initTimeString}</Text>
                            <Text style={styles.text}>➙</Text>
                            <Text style={styles.text}>{endTimeString}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            </GlobalContext.Consumer>
        );
    }

    render() {
        if (this.context.selectionMode) {
            return this.renderSelectionMode()
        } else {
            return this.renderDefault();
        }
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background,
        borderColor: Colors.Foreground,
        borderBottomWidth: 1,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "stretch",
    },
    containerSelectionMode: {
        backgroundColor: Colors.CurrentLine,
        borderBottomWidth: 0,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "stretch",
        marginVertical: 10,
        marginHorizontal: 30,
        borderRadius: 10
    },
    hourContainer: {
        alignItems: "flex-start",
        justifyContent: "space-between",
        flexDirection: "row",
        marginTop: 20
    },
    list: {
        margin: 10
    },
    text: {
        color: Colors.Foreground,
        fontSize: 40,
        marginHorizontal: 20,
        fontWeight: "bold",
    },
    dayLetterContainer: {
        width: 30,
        height: 30,
        alignItems: "center",
        justifyContent: "center",
        margin: 1,
    },
    dayLetter: {
        fontSize: 15
    }
})