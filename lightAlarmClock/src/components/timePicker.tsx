// import { TimePicker, } from "react-native-wheel-picker-android";
import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

import { Colors } from "../styles"


export default class MyPicker extends Component {
    state = {
    };

    onTimeSelected = date => {
        // this.setState({ date })
        this.props.updater(date)
    }

    constructor(props) {
        super(props)
        let date = new Date()
        try {
            date.setHours(props.hours)
            date.setMinutes(props.minutes)
        } catch {
            // hora inicial não 
            console.log('Erro na montage do time picker')
        }

        this.state = { date }
    }

    render() {
        console.log('render')
        console.log(this.state.date)
        return (
            <View style={styles.timeContainer}>
                {/* <TimePicker
                    onTimeSelected={this.onTimeSelected}
                    format24={true}
                    selectedItemTextColor={Colors.Purple}
                    indicatorColor={Colors.Background}
                    itemTextSize={20}
                    selectedItemTextSize={20}
                    initDate={this.state.date} /> */}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    timeContainer: {
        width: 100,
        color: Colors.Foreground,
        margin: 10,
    }
})
