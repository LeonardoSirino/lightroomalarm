import React from 'react'
import { useNavigation } from '@react-navigation/native';

import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import { Colors } from "../styles";

const AddButton = () => {
    const navigation = useNavigation()

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => {
                navigation.navigate('AlarmConfig', { newEntry: true })
            }}>

            <Text style={styles.text}>+</Text>
        </TouchableOpacity>
    )
}

export default AddButton


const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Foreground,
        width: 50,
        height: 50,
        borderRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        paddingBottom: 5,
        marginTop: 20,
        marginBottom: 20
    },
    text: {
        color: Colors.Background,
        fontSize: 40,
        margin: 0,
        fontWeight: "bold"
    }
})