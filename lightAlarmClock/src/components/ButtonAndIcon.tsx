import React from 'react'
import { useContext } from "react";
import { StyleSheet, TouchableOpacity, Image, GestureResponderEvent, ImageSourcePropType } from 'react-native'

import { Colors } from "../styles";
import { GlobalContext } from '../globalContext';

interface buttonAndIconPropType {
    iconSource: ImageSourcePropType,
    onPress: (evente: GestureResponderEvent) => void,
    style?: object
}

export default function ButtonAndIcon(props: buttonAndIconPropType) {
    const context = useContext(GlobalContext)

    let buttonIconStyle = { ...styles.buttonIcon }

    if (context.connected) {
        buttonIconStyle.opacity = 1
    } else {
        buttonIconStyle.opacity = 0.2
    }

    return (
        <TouchableOpacity
            style={{ ...styles.button, ...props.style }}
            onPress={context.connected ? props.onPress : () => {}}>
            <Image
                source={props.iconSource}
                style={buttonIconStyle} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: Colors.Background,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonIcon: {
        height: 30,
        resizeMode: "center",
        opacity: 1
    },
})