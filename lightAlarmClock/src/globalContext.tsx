import React from "react";

export const GlobalContext = React.createContext({
    connected: false,
    selectionMode: false,
    setContextData: () => {}
})