import React from 'react'
import Routes from './Routes';

import { useState } from "react";
import { GlobalContext } from './globalContext';

import { LogBox } from 'react-native';

export default function App() {
    const [contextData, setContextData] = useState({
        connected: false,
        selectionMode: false,
        setContextData: () => { }
    })

    LogBox.ignoreAllLogs();

    return (
        <GlobalContext.Provider value={{ ...contextData, setContextData }}>
            <Routes />
        </GlobalContext.Provider >
    )
}


