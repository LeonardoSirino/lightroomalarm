import { sha256 } from 'react-native-sha256';
import dgram from 'react-native-udp';

import { alarmDataType } from '../storage/types';
import { sleep } from '../util/timingFunctions';
import MessagesHandler from './messagesHandler';

export interface dgramRInfo {
    address: string,
    family: string,
    port: number,
    size: number
}

// only works for 8-bit chars
function toByteArray(obj) {
    var uint = new Uint8Array(obj.length);
    for (var i = 0, l = obj.length; i < l; i++) {
        uint[i] = obj.charCodeAt(i);
    }

    return new Uint8Array(uint);
}

export const TIMEOUT: number = 5000
const ESP_IDLE_MESSAGE = 'esp_idle_message'
const UDP_PORT = 3333

class UDPcom {
    socket: dgram.Socket
    esp_adress: string = ''
    last_message: string = ''
    last_message_timestamp = new Date()
    messagesHandler: MessagesHandler

    constructor() {
        this.messagesHandler = new MessagesHandler()
        // this.connect()
        //     .then(() => { console.log('Conexão estabelecida') })
        //     .catch(err => { console.log(err) })
    }

    connect = async () => {
        console.log('Conectando ao ESP')

        this.esp_adress = ''

        let socket = dgram.createSocket('udp4')
        socket.bind(UDP_PORT, '0.0.0.0', err => {
            console.log('Socket bind callback')
            console.error(err)
        })

        socket.on('message', this.messagesCB)
        this.socket = socket

        console.log('Waiting for ESP echo')

        let t0 = new Date()

        while (this.esp_adress === '') {
            await sleep(100);

            let t1 = new Date()
            let timeDiff = t1 - t0

            if (timeDiff >= TIMEOUT) {
                throw new Error('TIMEOUT da conexão')
            }
        }
    }

    // ANCHOR Callback for all received messages from UDP interface
    messagesCB = (data: Buffer, rinfo: dgramRInfo) => {
        let stringData = String.fromCharCode.apply(null, new Uint8Array(data))

        if (stringData === ESP_IDLE_MESSAGE && this.esp_adress === '') {
            this.esp_adress = rinfo.address
            console.log('Identificado echo do ESP')
        } else if (stringData !== ESP_IDLE_MESSAGE && stringData !== '') {
            console.log('Recebida mensagem')
            console.log(rinfo)

            let message = JSON.parse(stringData)
            message.rinfo = rinfo

            this.messagesHandler.newMessage(message)

            console.log(message)

            this.last_message = stringData
            this.last_message_timestamp = new Date()
        }
    }

    sendMessageCB = (info: any) => {
        if (info !== undefined) {
            console.log('Error in UDP send command')
            console.error(info)
        }
    }

    send = (value: object, operation: string) => {
        if (this.esp_adress === '') {
            throw new Error('Conexão ainda não estabelecida')
        }

        let stringValue = JSON.stringify({ ...value, operation })
        console.log('Mandando mensagem:')
        console.log(stringValue)

        let data = toByteArray(stringValue)
        this.socket.send(data, 0, data.length, UDP_PORT, this.esp_adress, this.sendMessageCB)
    }

    sendAndWaitConfirmation = async (value: object, operation: string) => {
        if (this.esp_adress === '') {
            throw new Error('Conexão ainda não estabelecida')
        }

        let stringValue = JSON.stringify({ ...value, operation })
        console.log('Mandando mensagem:')
        console.log(stringValue)

        let sendTime = new Date()

        let data = toByteArray(stringValue)
        let targetHash = await sha256(stringValue)
        console.log('Hash da mensagem:')
        console.log(targetHash)
        this.socket.send(data, 0, data.length, UDP_PORT, this.esp_adress, this.sendMessageCB)

        await this.messagesHandler.waitForHash(targetHash, sendTime)
    }

    sendAndWaitResponse = async (value: object, operation: string) => {
        if (this.esp_adress === '') {
            throw new Error('Conexão ainda não estabelecida')
        }

        let stringValue = JSON.stringify({ ...value, operation })
        console.log('Mandando mensagem:')
        console.log(stringValue)

        let sendTime = new Date()

        let data = toByteArray(stringValue)
        this.socket.send(data, 0, data.length, UDP_PORT, this.esp_adress, this.sendMessageCB)

        const response = await this.messagesHandler.waitForResponse(operation, sendTime)

        return response
    }
}

class EspSocketInterface {
    udpcom: UDPcom

    constructor() {
        this.udpcom = new UDPcom()
    }

    connect = async () => {
        await this.udpcom.connect()
    }
}

export class LightControlInterface extends EspSocketInterface {
    constructor() {
        super()
    }

    sendLightIntensity = (lightIntensity: number) => {
        this.udpcom.send({ lightIntensity }, 'setLightIntensity')
    }

    sendLightStatus = async (lightStatus: boolean) => {
        await this.udpcom.sendAndWaitConfirmation({ lightStatus }, 'setLightStatus')
    }

    getLightIntensity = async () => {
        const { lightIntensity } = await this.udpcom.sendAndWaitResponse({}, 'getLightIntensity')
        console.log(`Response from ESP32 for light intensity: ${lightIntensity}`)

        return lightIntensity
    }
}

export class AlarmDataInterface extends EspSocketInterface {
    constructor() {
        super()
    }

    sendAllAlarmData = async (allAlarmData: Array<alarmDataType>) => {
        console.log('Recebida requisição de upload dos dados')
        console.log(allAlarmData)
        await this.udpcom.sendAndWaitConfirmation({ allAlarmData }, 'setAlarmData')
    }
}