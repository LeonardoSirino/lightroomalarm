import { sleep } from '../util/timingFunctions';
import { dgramRInfo, TIMEOUT } from './espCommunication';

const MAX_UNREAD_MESSAGES = 50


interface message {
    operation: string,
    timestamp: Date,
    read: boolean,
    rinfo: dgramRInfo,
    hash: string,
    lightIntensity: number
}


export default class MessagesHandler {
    messages: Array<message> = []
    lastMessage: message

    constructor() {
        this.lastMessage = {
            operation: 'string',
            timestamp: new Date(),
            read: false,
            rinfo: {}
        }
    }

    newMessage = (msg: message) => {
        msg.timestamp = new Date()
        msg.read = false

        this.messages.push(msg)
    }

    waitForHash = async (hash: string, sendTime: Date) => {
        let t0 = new Date()

        while (true) {
            // TODO Implement checks only for not read messages
            let match = this.messages.find(msg => msg.operation == 'hash' && msg.timestamp > sendTime && msg.hash === hash)

            if (match != undefined) {
                console.log('Match of messagesHandler')
                console.log(match)

                return true
            }

            await sleep(100)

            let t1 = new Date()
            let timeDiff = t1 - t0

            if (timeDiff >= TIMEOUT) {
                throw new Error('TIMEOUT da mensagem')
            }
        }
    }

    waitForResponse = async (operation: string, sendTime: Date) => {
        let t0 = new Date()

        while (true) {
            // TODO Implement checks only for not read messages
            let match = this.messages.find(msg => msg.operation == operation && msg.timestamp > sendTime)

            if (match != undefined) {
                console.log('Match of messagesHandler')
                console.log(match)

                return match
            }

            await sleep(100)

            let t1 = new Date()
            let timeDiff = t1 - t0

            if (timeDiff >= TIMEOUT) {
                throw new Error('TIMEOUT waiting for message response')
            }
        }
    }
}