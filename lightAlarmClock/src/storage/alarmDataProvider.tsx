import AsyncStorage from '@react-native-community/async-storage';

import { alarmDataType } from "./types";

export class AlarmDataProvider {
    allAlarmData: Array<alarmDataType> = []
    IDs: Array<number> = []


    getAllAlarmData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('@allAlarmData')
            this.allAlarmData = JSON.parse(jsonValue)
        } catch (e) {
            console.log('Erro na leitura do storage')
        }

        return this.allAlarmData
    }

    listIds = async () => {
        await this.getAllAlarmData()

        let IDs: Array<number> = []
        if (this.allAlarmData !== null) {
            IDs = this.allAlarmData.map(item => item.id)
        }

        this.IDs = IDs

        console.log('Lista de IDs')
        console.log(this.IDs)
        return IDs
    }

    newId = async () => {
        await this.listIds()
        if (this.IDs.length === 0) {
            return 0
        }

        let maxID: number = Math.max(...this.IDs)
        return maxID + 1
    }

    updateAlarmData = async (alarmData: alarmDataType) => {
        try {
            this.validateAlarmData(alarmData)
        } catch (err) {
            throw err
        }
        console.log('Provider AlarmData')
        console.log(alarmData)

        if (this.allAlarmData == null) {
            console.log('Objeto não encontrado')
            this.allAlarmData = [alarmData]
        } else {
            try {
                let index = this.findElementIndex(alarmData);

                console.log('index')
                console.log(index)

                this.allAlarmData[index] = alarmData
            }
            catch{
                console.log('Append do objeto')
                console.log('Dados na memória')
                console.log(this.allAlarmData)
                console.log('Novos dados')
                console.log(alarmData)
                console.log('Fim do append')
                this.allAlarmData = [...this.allAlarmData, alarmData]
            }
        }

        await AsyncStorage.setItem('@allAlarmData', JSON.stringify(this.allAlarmData))
        this.listIds()
    }

    clearStorage = async () => {
        console.log('Clear storage')
        await AsyncStorage.setItem('@allAlarmData', '')

        this.allAlarmData = []
        this.IDs = []
    }

    deleteElement = async (item: alarmDataType) => {
        console.log('Recebeu elemento para se deletar')
        console.log(item)

        try {
            let index = this.findElementIndex(item)
            this.allAlarmData.splice(index, 1)
            this.IDs.splice(index, 1)

            await AsyncStorage.setItem('@allAlarmData', JSON.stringify(this.allAlarmData))
        }
        catch{
            console.log('Falha ao deletar elemento')
        }

    }

    private findElementIndex(alarmData: alarmDataType) {
        let matched = this.allAlarmData.find(item => item.id === alarmData.id);
        if (matched === undefined) {
            throw new Error('Elemento não encontrado');
        }

        let id = matched.id;
        let index = this.IDs.indexOf(id);
        return index;
    }

    private validateAlarmData(alarmData: alarmDataType) {
        if (alarmData.endMinutes <= alarmData.initMinutes) {
            throw new Error('O horário final do despertador deve ser maior que o inicial')
        }
    }
}