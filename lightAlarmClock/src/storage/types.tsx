export interface alarmDataType {
    id: number,
    days: [boolean, boolean, boolean, boolean, boolean, boolean, boolean],
    initMinutes: number,
    endMinutes: number
}