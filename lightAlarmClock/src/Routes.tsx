import React from 'react'
import Home from './pages/home'
import './config/StatusBarConfig'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AlarmConfig from './pages/alarmConfig';
import LightControl from './pages/lightControl';

const Stack = createStackNavigator();

export default function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home"
                    component={Home}
                    options={{
                        headerShown: false
                    }} />

                <Stack.Screen name="AlarmConfig"
                    component={AlarmConfig}
                    options={{
                        headerShown: false
                    }} />

                <Stack.Screen name="LightControl"
                    component={LightControl}
                    options={{
                        headerShown: false
                    }} />

            </Stack.Navigator>
        </NavigationContainer>
    )
}
