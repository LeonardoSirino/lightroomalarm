import React, { Component } from 'react';
import { Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import TimePicker from '../components/timePicker';
import { AlarmDataProvider } from '../storage/alarmDataProvider';
import { alarmDataType } from '../storage/types';
import { Colors } from '../styles';
import { minutesToHourAndMinutes, nativeDateToMinutes } from '../util/timeConversions';
import { sleep } from '../util/timingFunctions';

let alarmDataProvider = new AlarmDataProvider()

export default class AlarmConfig extends Component {
    state = {
        alarmData: {
            id: -1,
            days: [false, true, true, true, true, true, false],
            initMinutes: 7 * 60,
            endMinutes: 7.5 * 60
        },
        initTime: new Date(),
        endTime: new Date()
    }

    componentDidMount = async () => {
        // Inicialização da memória dessa instância
        await alarmDataProvider.getAllAlarmData()

        let alarmData: alarmDataType
        if (!this.props.route.params.newEntry) {
            alarmData = this.props.route.params.alarmData
        } else {
            let newID = await alarmDataProvider.newId()
            console.log('Novo ID')
            console.log(newID)
            alarmData = {
                id: newID,
                days: [false, true, true, true, true, true, false],
                initMinutes: 7 * 60,
                endMinutes: 7.5 * 60
            }
        }

        var { hour, minute } = minutesToHourAndMinutes(alarmData.initMinutes)
        let initTime = new Date()
        initTime.setHours(hour)
        initTime.setMinutes(minute)

        var { hour, minute } = minutesToHourAndMinutes(alarmData.endMinutes)
        let endTime = new Date()
        endTime.setHours(hour)
        endTime.setMinutes(minute)

        this.setState({ alarmData, initTime, endTime })
    }

    updateInitTime = (date: Date) => {
        this.state.initTime = date

        let minutes = nativeDateToMinutes(date)
        this.state.alarmData.initMinutes = minutes
    }

    updateEndTime = (date: Date) => {
        this.state.endTime = date

        let minutes = nativeDateToMinutes(date)
        this.state.alarmData.endMinutes = minutes
    }

    backToHome = () => {
        this.props.navigation.navigate('Home')
    }

    saveAlarmConfig = async () => {
        console.log('Salvando dados da configuração do alarme')
        // this sleep ensures that wheel picker will stop moving before unmounting this page
        // this function could be optimized if there is a state that indicates the moviment of the wheel
        await sleep(200);
        alarmDataProvider.updateAlarmData(this.state.alarmData)
            .then(this.backToHome)
            .catch((err) => { Alert.alert('Erro', err.message) })
    }

    updateDayStatus = (id: number, status: boolean) => {
        let { alarmData } = this.state

        alarmData.days[id] = !status

        this.setState({ alarmData })
    }

    renderDay = ({ item }) => {
        let buttonStyle = { ...styles.dayButton }
        let textStyle = { ...styles.dayButtonText }

        if (item.status) {
            textStyle.color = Colors.Foreground
            textStyle.opacity = 1
            buttonStyle.backgroundColor = Colors.Purple
        } else {
            textStyle.color = Colors.Foreground
            textStyle.opacity = 0.5
            buttonStyle.backgroundColor = Colors.Background
        }

        return (
            <TouchableOpacity
                style={buttonStyle}
                onPress={() => this.updateDayStatus(item.id, item.status)}>
                <Text style={textStyle}>
                    {item.dayCode}
                </Text>
            </TouchableOpacity>
        )
    }

    deleteAlarmConfig = async () => {
        console.log('Deletou')
        await alarmDataProvider.deleteElement(this.state.alarmData)
        this.backToHome()
    }

    render() {
        let initHour = this.state.initTime.getHours()
        let initMinute = this.state.initTime.getMinutes()

        let endHour = this.state.endTime.getHours()
        let endMinute = this.state.endTime.getMinutes()

        let daysCodes = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S']
        let daysData = []

        try {
            daysData = this.state.alarmData.days.map((item, index) => {
                let data = {
                    id: index,
                    dayCode: daysCodes[index],
                    status: item
                }

                return data
            })
        } catch{
            return <View />
        }

        return (
            <View style={styles.container}>
                <View style={styles.pickersContainer}>
                    <TimePicker
                        updater={this.updateInitTime}
                        hours={initHour}
                        minutes={initMinute} />

                    <Text style={styles.text}>
                        até
                    </Text>

                    <TimePicker
                        updater={this.updateEndTime}
                        hours={endHour}
                        minutes={endMinute} />
                </View>

                <FlatList
                    data={daysData}
                    renderItem={this.renderDay}
                    horizontal={true}
                    style={styles.daysContainer}
                />

                <View style={styles.buttonsContainer}>
                    <TouchableOpacity
                        style={{ ...styles.button, backgroundColor: Colors.Orange }}
                        onPress={() => {
                            this.saveAlarmConfig()
                                .then(() => { console.log('Configurações atualizadas') })
                                .catch(() => { console.log('Erro na atualização das configurações') })
                        }}>

                        <Text style={styles.buttonText}>Salvar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ ...styles.button, backgroundColor: Colors.Purple }}
                        onPress={() => { this.backToHome() }}>

                        <Text style={styles.buttonText}>Cancelar</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                    style={!this.props.route.params.newEntry ? styles.deleteButton : { ...styles.deleteButton, opacity: 0.2 }}
                    onPress={!this.props.route.params.newEntry ? this.deleteAlarmConfig : () => { }}>
                    <Image source={require('../assets/icons/trash_can.png')} style={styles.deleteButtonIcon} />
                </TouchableOpacity>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background,
        color: Colors.Foreground,
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    text: {
        color: Colors.Foreground,
        fontSize: 20,
        margin: 10
    },
    button: {
        height: 50,
        width: 150,
        backgroundColor: Colors.Purple,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
        margin: 10
    },
    buttonsContainer: {
        flexDirection: "row",
        margin: 0
    },
    buttonText: {
        color: Colors.Foreground,
        fontSize: 20,
        fontWeight: "bold"
    },
    pickersContainer: {
        flexDirection: "row",
        alignItems: "center",
        margin: 0
    },
    dayButton: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: Colors.Foreground,
        margin: 6,
        alignItems: "center",
        justifyContent: "center"
    },
    daysContainer: {
        marginVertical: 40,
        flexGrow: 0
    },
    dayButtonText: {
        margin: 0,
        padding: 0,
        fontWeight: "bold",
        fontSize: 15,
    },
    deleteButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: Colors.Background,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "flex-end",
        position: 'absolute',
        bottom: 20,
        right: 20,

    },
    deleteButtonIcon: {
        height: 30,
        resizeMode: "center",
    }
})

