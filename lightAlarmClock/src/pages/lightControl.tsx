import React, { Component } from 'react';
import { Image, Slider, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { LightControlInterface } from '../EspSocket/espCommunication';
import { Colors } from '../styles';

export default class LightControl extends Component {
    lightControlInterface: LightControlInterface

    state = {
        active: false,
        intensity: 50,
    }

    constructor(props) {
        super(props)
        this.lightControlInterface = new LightControlInterface()
        this.connectToLightControlInterface()
            .then(() => { console.log('Conexão estabelecida no controle manual da luz') })
    }

    connectToLightControlInterface = async () => {
        await this.lightControlInterface.connect()
        const lightIntensity = await this.lightControlInterface.getLightIntensity()

        this.setState({ intensity: lightIntensity })
    }

    sendLightIntensity = (value) => {
        this.lightControlInterface.sendLightIntensity(value)
    }

    toggleLightSwitch = () => {
        let currentStatus = this.state.active
        this.setState({ active: !currentStatus })
        
        // FIXME Temp correction, the real correction should be implemented in ESP32
        if (!currentStatus) {
            this.lightControlInterface.sendLightIntensity(0)
        }

        this.lightControlInterface.sendLightStatus(!currentStatus)
            .then(() => { console.log('Mensagem recebida com sucesso') })
            .catch(() => { console.log('Erro no envio da mensagem') })
    }

    render() {
        return (
            <View style={styles.container}>
                <Slider
                    step={1}
                    maximumValue={100}
                    style={styles.sliderControl}
                    value={this.state.intensity}
                    minimumTrackTintColor={Colors.Yellow}
                    thumbTintColor={Colors.Yellow}
                    onValueChange={(value) => { this.sendLightIntensity(value) }}
                />
                <TouchableOpacity
                    style={this.state.active ? styles.lightButton : { ...styles.lightButton, opacity: 0.4 }}
                    onPress={this.toggleLightSwitch}>
                    <Image source={require('../assets/icons/light_bulb.png')} style={styles.lightButtonIcon} />
                    <Text style={styles.lightButtonText}>{this.state.active ? 'Apagar' : 'Acender'}</Text>
                </TouchableOpacity>
            </View>
        )

    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background,
        color: Colors.Foreground,
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    text: {
        color: Colors.Foreground,
        fontSize: 30
    },
    alarmBlockList: {
        backgroundColor: Colors.Background,
        flexGrow: 0
    },
    lightButton: {
        width: 300,
        // height: 50,
        borderRadius: 25,
        backgroundColor: Colors.Background,
        alignItems: "center",
        justifyContent: "center",
        // flexDirection: "column",
        marginTop: 30
    },
    lightButtonText: {
        color: Colors.Yellow,
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 10
    },
    lightButtonIcon: {
        height: 30,
        resizeMode: "contain",

    },
    sliderControl: {
        width: 300,
        margin: 20
    }
})