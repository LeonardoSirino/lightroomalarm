import axios from 'axios';
import React, { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Colors } from '../styles';

const Temp: React.FC = () => {
    useEffect(() => {
        console.log('Componente montou')

        const api = axios.create({
            baseURL: 'http://lightroomalarm.local'
        })

        api.get('/')
            .then(() => { console.log('request API') })
            .catch(err => { console.error(err) })

    })

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Temporário</Text>
        </View>
    )
}

export default Temp;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background,
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    text: {
        color: Colors.Foreground,
        fontSize: 40
    }
})
