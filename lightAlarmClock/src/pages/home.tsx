import React, { Component } from 'react';
import { Alert, FlatList, StyleSheet, View } from 'react-native';

import AddButton from '../components/addButton';
import AlarmBlock from '../components/alarmBlock';
import ButtonAndIcon from '../components/ButtonAndIcon';
import { AlarmDataInterface } from '../EspSocket/espCommunication';
import { GlobalContext } from '../globalContext';
import { AlarmDataProvider } from '../storage/alarmDataProvider';
import { alarmDataType } from '../storage/types';
import { Colors } from '../styles';

let alarmDataProvider = new AlarmDataProvider()

export default class Home extends Component {
    alarmDataInterface: AlarmDataInterface

    state = {
        allAlarmData: []
    }

    constructor(props) {
        super(props)
        // alarmDataProvider.clearStorage()
        this.alarmDataInterface = new AlarmDataInterface()
        this.alarmDataInterface.connect()
            .then(() => {
                console.log('Conexão estabelecida')
                this.context.setContextData({ connected: true })
            })
            .catch(err => {
                console.log('Home não conseguiu conectar')
                console.error(err)
            })

    }

    componentDidMount = () => {
        // Definição de callback para toda vez que a janela é renderizada novamente
        this.props.navigation.addListener('focus', () => { this.updateScreen() })
        this.updateScreen()
    }

    updateScreen = async () => {
        console.log('Home montou')

        let allAlarmData = await alarmDataProvider.getAllAlarmData()
        console.log(allAlarmData)
        this.setState({ allAlarmData })
    }

    renderItem = ({ item }) => {
        console.log('Iteração HOME')
        console.log(item)
        return (
            <AlarmBlock
                editAlarmScreen={() =>
                    this.props.navigation.navigate('AlarmConfig',
                        {
                            newEntry: false,
                            alarmData: item
                        }
                    )}
                alarmData={item}
            />
        )
    }

    sendAlarmData = async () => {
        console.log('Upload dos dados')
        try {
            await this.alarmDataInterface.sendAllAlarmData(this.state.allAlarmData)
            Alert.alert('Upload concluído!', 'O ESP recebeu os dados da nova configuração de alarme')
        } catch{
            Alert.alert('Erro!', 'Erro no upload dos dados para o ESP')
        }
    }

    render() {
        let { allAlarmData } = this.state
        console.log('Montando HOME')
        console.log(allAlarmData)

        return (
            <View style={styles.container}>
                <FlatList
                    data={allAlarmData}
                    renderItem={this.renderItem}
                    style={styles.alarmBlockList}
                    keyExtractor={item => item.id.toString()} />
                <AddButton />

                <View style={styles.comButtonsContainer}>
                    <ButtonAndIcon
                        onPress={this.sendAlarmData}
                        iconSource={require('../assets/icons/upload.png')}
                    />

                    <ButtonAndIcon
                        onPress={() => console.log('download')}
                        iconSource={require('../assets/icons/download.png')}
                    />
                </View>

                <ButtonAndIcon
                    onPress={() => this.props.navigation.navigate('LightControl')}
                    iconSource={require('../assets/icons/light_bulb.png')}
                    style={styles.lightButton}
                />

            </View>
        )
    }
}

Home.contextType = GlobalContext;


const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.Background,
        color: Colors.Foreground,
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    text: {
        color: Colors.Foreground,
        fontSize: 30
    },
    alarmBlockList: {
        backgroundColor: Colors.Background,
        flexGrow: 0
    },
    button: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: Colors.Background,
        alignItems: "center",
        justifyContent: "center",
    },
    lightButton: {
        alignSelf: "flex-end",
        position: 'absolute',
        bottom: 20,
        right: 20,
    },
    comButtonsContainer: {
        flexDirection: "row",
        width: 100,
        bottom: 20,
        left: 15,
        position: "absolute",
        margin: 0,
    }
})